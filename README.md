# Mentor-Finder

This Project is created to establish connetion between Mentee and Mentor in Skills which their Studies in needed to improve and improvise.

Team Size : 5 persons.

Project size : 90 days.

Technologies : Java, Boostrap 4, Spring Framework, Websocket, Sql Server, TomCat server, Thymleaf.

# Version

## 12/7/2021
Sửa bảng Enrolled, thêm cột status, hoàn thiện chức năng enrolled, accept reject enrolled

## 7/7/2021
Sửa bảng Subject, thêm cột hình ảnh minh họa của môn học

## 17/6/2021
Sửa lại bảng Request, xong sơ bộ function Create Request

## 15/6/2021
Từ giờ tạo user để làm clg thì cứ register nhé.

## 6/10/2021
Phân quyền admin và user thành công
Giữ User Context
Đối chiếu entity từ bảng
Update table

## 3/6/2021
Thay đổi hoàn toàn sang Spring Boot, tích hợp thành công View và JPA, basic Entity managing.
FR: Login & Register.
## 27/5/2021 
Basic implementation of Spring MVC: điền form và controller đơn giản.

